# -*- coding: utf-8 -*-
import argparse
import os
import pdb
from glob import glob

from skimage.io import imread

from skimage.measure import compare_psnr

import matplotlib.pyplot as plt

import numpy as np


if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument('--inFolder',type=str,help="input folder where the images are")
	parser.add_argument('--outFolder',type=str,help=" output folder where the images go")
	
	# argument parsing
	args = parser.parse_args()
	inFolder = args.inFolder
	outFolder = args.outFolder

	psnrList = []
	
	for root, dirs, files in os.walk(inFolder):
		for file in files:

			fname = os.path.join(root, file)
			print("Processing image "+str(fname))

			try:
				print("reading the signal image")
				original = imread(os.path.join(outFolder, "out_"+str(file)), as_gray=True)

			except Exception as e :
				print("no signal image corresponding to that noise image")
				continue


			

			print("reading the noise image")
			noisy = imread(fname, as_gray=True)
	
			print("estimating psnr")
			maxValue = original.max()
			if noisy.max() > maxValue:
				maxValue = noisy.max()

			minValue = original.min()
			if noisy.min() < minValue :
				minValue = noisy.min()

			psnr = compare_psnr(original,noisy,data_range=maxValue-minValue)



			psnrList.append(psnr)
			

			

	psnrArray = np.array(psnrList)

	plt.hist(psnrArray, bins=1000)  # arguments are passed to np.histogram
	plt.title("Histogram of PSNR")


	plt.savefig('psnrHistogram.png')



			











