# -*- coding: utf-8 -*-
import argparse
import os
import pdb
from glob import glob
import random

import numpy as np

from skimage.transform import (hough_line, hough_line_peaks,
                               probabilistic_hough_line)
from skimage.feature import canny
from skimage import data

from skimage.io import imread,imsave

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib import cm
import pdb

from skimage.measure import label ,  regionprops

from skimage.restoration import inpaint


if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument('--inFolder',type=str,help="input folder where the images are")
	parser.add_argument('--outFolder',type=str,help=" output folder where the images go")
	
	# argument parsing
	args = parser.parse_args()
	inFolder = args.inFolder
	outFolder = args.outFolder
	
	for root, dirs, files in os.walk(inFolder):
		for file in files:

			try:
				fname = os.path.join(root, file)
				print("Processing image "+str(fname))

				print("reading the image")
				image = imread(fname, as_gray=True)
				
				# initialize the mask to inpaint
				mask = np.zeros(image.shape)

				print("detect edges using canny")
				edges = canny(image,1.5,0.2*image.max(),0.4*image.max())


				# generate a labelized image to extract bounding boxes
				label_image = label(edges)

				for region in regionprops(label_image):
					# take regions not so big, considering just small artifacts
					if region.area <= (0.0004 * (image.shape[0] * image.shape[1])):  

						minr, minc, maxr, maxc = region.bbox

						rowDif = maxr - minr
						colDif = maxc - minc

						if rowDif > 2*colDif or colDif > 2*rowDif :
							# if the bboxes are too tall or too width, they are discarded
							print("Too much aspect ratio")
						else:

							# set this rectangle of the image as to be masked because it may contain an artifact
							mask[minr:maxr, minc:maxc] = 1


				print("inpainting the original image with the mask")
				image_result = inpaint.inpaint_biharmonic(image, mask)

				print("save it to an output file")
				imsave(os.path.join(outFolder, "out_"+str(file)),image_result)
			except Exception as e :
				print("couldnt process the image")
				continue










