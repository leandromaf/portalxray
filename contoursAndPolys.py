import numpy as np
import matplotlib.pyplot as plt

from skimage.draw import ellipse
from skimage.measure import find_contours, approximate_polygon, \
    subdivide_polygon

from skimage.io import imread

from skimage.filters import threshold_adaptive , threshold_mean


fname = "images/images/00000004_000.png"

image = imread(fname, as_gray=True)

block_size = 5
thresh = threshold_adaptive(image, block_size, offset=10)

# thresh = threshold_mean(image)
img = image > thresh

plt.gray()
plt.imshow(img)

# approximate / simplify coordinates of the two ellipses
for contour in find_contours(img, 0):
    coords = approximate_polygon(contour, tolerance=2.5)
    plt.plot(coords[:, 1], coords[:, 0], '-r', linewidth=2)
    coords2 = approximate_polygon(contour, tolerance=39.5)
    plt.plot(coords2[:, 1], coords2[:, 0], '-g', linewidth=2)
    print("Number of coordinates:", len(contour), len(coords), len(coords2))



plt.show()